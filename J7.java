// 3.6.21

import java.util.Scanner;

public class J7 {

    public static int countFour(int number) {

        number = Math.abs(number);
        int count = 0;

        while (number != 0) {
            if (number % 10 == 4) {
                count++;
            }
            number = number / 10;
        }
        return count;
    }

    public static void main(String[] args) {

        System.out.println("Number?");
        Scanner scan = new Scanner(System.in);
        int number = scan.nextInt();
        scan.close();
        System.out.println("Count of 4 in " + number + " = " + countFour(number));
    }
}
