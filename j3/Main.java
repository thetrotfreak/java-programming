package j3;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        boolean persistentMenu = true;
        int option;
        int arg;

        while (persistentMenu) {
            Menu.printMenu();
            System.out.print("Option > ");
            option = scan.nextInt();
            System.out.print("Number > ");
            arg = scan.nextInt();

            switch (option) {

                case 1:
                    String m1 = NumberUtility.isPrime(arg) ? "PRIME" : "COMPOSITE";
                    System.out.println("> " + m1);
                    break;

                case 2:
                    System.out.println("> " + NumberUtility.factors(arg));
                    break;

                case 3:
                    String m3 = NumberUtility.isODD(arg) ? "ODD" : "EVEN";
                    System.out.println("> " + m3);

                    break;
                case 4:
                    System.out.println("> " + NumberUtility.reverse(arg));
                    break;

                default:
                    persistentMenu = false;
                    System.out.println("---EXITED---");
                    break;
            }
        }
        scan.close();
    }
}
