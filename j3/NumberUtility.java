package j3;

public class NumberUtility {

    public static boolean isPrime(int n) {
        if (n < 2)
            return false;

        for (int i = 2; i * i <= n; i++) {
            if (n % i == 0)
                return false;
        }

        return true;
    }

    public static String factors(int n) {

        if (n == 0)
            return "0 has infinitely many factors";

        StringBuilder factorList = new StringBuilder();
        factorList.trimToSize();

        for (int i = 1; i <= n; i++)
            if (n % i == 0) {
                factorList.append(i);
                factorList.append(" ");
            }

        factorList.trimToSize();

        return factorList.toString();
    }

    public static boolean isODD(int n) {
        return (n % 2 != 0) ? true : false;
    }

    public static int reverse(int n) {

        int d = 0;
        int r = 0;

        while (n != 0) {
            d = n % 10;
            r = r * 10 + d;
            n = n / 10;
        }
        return r;
    }

    // following isn't a part of question
    public static long gcd(long number1, long number2) {
        // GCD is only defined for positive integers
        if (number1 < 0)
            number1 = number1 * -1;

        if (number2 < 0)
            number2 = number2 * -1;

        if (number1 == 0)
            return number2;

        if (number2 == 0)
            return number1;

        if (number1 == number2)
            return number1;

        return gcd(number2, number1 % number2);
    }

    // following isn't a part of question
    public static long lcm(long number1, long number2) {
        // lcm is only defined for positive integers
        if (number1 < 0)
            number1 *= -1;

        if (number2 < 0)
            number2 *= -1;

        // return (number1 * number2) / gcd(number1, number2);
        return (Math.max(number1, number2) / gcd(number1, number2)) * Math.min(number1, number2);
    }
}
