package j3;

public class Menu {

    public static void printMenu() {

        System.out.println();
        System.out.println("---MENU---");
        System.out.println("Press 1 for Primality");
        System.out.println("Press 2 for Factors");
        System.out.println("Press 3 for Odd or Even");
        System.out.println("Press 4 for Reverse");
        System.out.println();
    }

}
