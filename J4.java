// 21-5-21

import java.util.Scanner;

public class J4 {

    public static String hexOf(int n10) {

        if (n10 <= 9 && n10 >= -9)
            return String.valueOf(n10);

        StringBuilder n16 = new StringBuilder();
        int digit;

        while (n10 != 0) {

            digit = n10 % 16;

            if (digit > 9) {
                // adding the constant 55 to get ASCII value
                // eg. 13 + 55 = 68
                // 68 in ASCCI is D
                int n = digit + 55;
                char c = (char) n;
                n16.append(c);
            } else
                n16.append(digit);
            n10 = n10 / 16;
        }
        return n16.reverse().toString();
    }

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.print("DECIMAL : ");
        int n10 = scan.nextInt();
        scan.close();
        System.out.println("HEX : " + J4.hexOf(n10));
    }
}
