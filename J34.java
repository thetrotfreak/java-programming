public class J34 {
    public static void main(String[] args) {

        int[] ilist = new int[args.length];

        for (int i = 0; i < args.length; i++) {
            ilist[i] = Integer.valueOf(args[i]);
        }

        System.out.println("Duplicate Entries :");
        boolean hasDuplicates = false;

        for (int i = 0; i < ilist.length - 1; i++) {

            for (int j = i + 1; j < ilist.length; j++) {

                if (ilist[i] == ilist[j]) {
                    // feature add number of repitions
                    hasDuplicates = true;
                    System.out.print(ilist[j] + " ");
                }
            }
        }
        if (!hasDuplicates) {
            System.out.println("none");
        }
    }
}
