// 4.6.21

import java.util.Scanner;

public class J10 {

    public static void fillPascalTrinagle(int[][] pascal) {

        // right biased

        for (int i = 0; i < pascal.length; i++) {

            for (int j = 0; j <= i; j++) {

                if (j == 0 || j == i)
                    pascal[i][j] = 1;

                else
                    pascal[i][j] = pascal[i - 1][j] + pascal[i - 1][j - 1];
            }
        }

    }

    public static void printPascalTriangle(int[][] pascal) {

        System.out.println("---Pascal's Trinagle---");

        for (int i = 0; i < pascal.length; i++) {

            for (int j = 0; j <= i; j++)
                System.out.print(pascal[i][j] + " ");

            System.out.println();
        }

    }

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Number of Rows?");
        int rows = scan.nextInt();

        scan.close();

        int[][] matrix = new int[rows][rows];

        fillPascalTrinagle(matrix);
        printPascalTriangle(matrix);
    }
}