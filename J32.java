import java.util.Arrays;

public class J32 {
    public static int binarySearch(int[] a, int key) {
        Arrays.sort(a);

        int l = 0;
        int r = a.length - 1;

        while (l <= r) {
            int m = l + (r - l) / 2;
            int me = a[m];

            if (me < key)
                l = m + 1;
            else if (me > key)
                r = m - 1;
            else
                return m;
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] a = { 1, 2, -6, -1, 0, 9, 1, 8, 4, 2 };
        System.out.print(binarySearch(a, 1));
    }
}