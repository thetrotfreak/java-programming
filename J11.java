public class J11 {

    public static void fillMatrix(String[] clargs, int[][] A, int[][] B) {

        int k = 0;

        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A[i].length; j++)
                A[i][j] = Integer.valueOf(clargs[k++]);

        }

        for (int i = 0; i < B.length; i++) {
            for (int j = 0; j < B[i].length; j++)
                B[i][j] = Integer.valueOf(clargs[k++]);

        }
    }

    public static void addMatrix(int[][] A, int[][] B) {

        int[][] C = new int[3][3];

        for (int i = 0; i < C.length; i++) {

            for (int j = 0; j < C[i].length; j++)
                C[i][j] = A[i][j] + B[i][j];

        }
        print(C);
    }

    public static void subMatrix(int[][] A, int[][] B) {

        int[][] C = new int[3][3];

        for (int i = 0; i < C.length; i++) {

            for (int j = 0; j < C[i].length; j++)
                C[i][j] = A[i][j] - B[i][j];

        }
        print(C);
    }

    public static void mulMatrix(int[][] A, int[][] B) {

        int[][] C = new int[3][3];

        for (int i = 0; i < A.length; i++) {

            for (int j = 0; j < B.length; j++) {

                for (int k = 0; k < C.length; k++)
                    C[i][j] = C[i][j] + A[i][k] * B[k][j];

            }
        }
        print(C);
    }

    public static void transMatrix(int[][] A) {

        int[][] AT = new int[3][3];

        for (int i = 0; i < A.length; i++) {

            for (int j = 0; j < A[i].length; j++)
                AT[j][i] = A[i][j];
        }

        print(AT);
    }

    public static void print(int[][] A) {

        for (int[] a : A) {
            for (int e : a) {
                String p = e < 0 ? "" : " ";
                System.out.print(p + e + "\t");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {

        int[][] A = new int[3][3];
        int[][] B = new int[3][3];

        fillMatrix(args, A, B);

        System.out.println("Matrix A:");
        print(A);

        System.out.println("\nMatrix B:");
        print(B);

        System.out.println("\nA + B:");
        addMatrix(A, B);

        System.out.println("\nA - B:");
        subMatrix(A, B);

        System.out.println("\nA x B:");
        mulMatrix(A, B);

        System.out.println("\nT(A):");
        transMatrix(A);

        System.out.println("\nT(B):");
        transMatrix(B);
    }
}
