import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class J12 {
    private static final String DELIMITER_STRING = "..";

    public static int countWord(String[] textRef) {

        if (textRef == null || textRef.length == 0)
            return 0;

        String[] words;
        int wordCount = 0;

        for (int i = 0; i < textRef.length; i++) {

            if (textRef[i].equals(DELIMITER_STRING))
                break;

            words = textRef[i].split("\\s+");

            for (int j = 0; j < words.length; j++) {
                // use isBlank() to not count String composed of only whitespaces
                if (!words[j].isBlank())
                    wordCount++;
            }
        }

        return wordCount;
    }

    public static int countChar(String[] textRef) {

        if (textRef == null || textRef.length == 0)
            return 0;

        String[] words;
        int charCount = 0;

        for (int i = 0; i < textRef.length; i++) {

            if (textRef[i].equals(DELIMITER_STRING))
                break;

            words = textRef[i].split("\\s+");

            for (int j = 0; j < words.length; j++) {
                // use isBlank() to not count String composed of only whitespaces
                if (!words[j].isBlank())
                    charCount = charCount + words[j].length();
            }
        }

        return charCount;
    }

    public static int countVowels(String[] textRef) {

        if (textRef == null || textRef.length == 0)
            return 0;

        String[] words;
        String[] vowels = { "a", "e", "i", "o", "u" };

        int vowelCount = 0;

        for (int i = 0; i < textRef.length; i++) {

            if (textRef[i].equals(DELIMITER_STRING))
                break;

            words = textRef[i].split("\\s+");

            for (int j = 0; j < words.length; j++) {

                for (int k = 0; k < words[j].length(); k++) {

                    String c = Character.toString(words[j].charAt(k));

                    for (int l = 0; l < vowels.length; l++) {

                        if (c.equalsIgnoreCase(vowels[l]))
                            vowelCount++;
                    }
                }
            }
        }

        return vowelCount;
    }

    public static void main(String[] args) throws IOException {

        BufferedReader buffRef = new BufferedReader(new InputStreamReader(System.in));
        String[] text = new String[125];

        System.out.println("Write some Text?");
        System.out.println("Use .. (double periods) to stop writing!");

        for (int i = 0; i < text.length; i++) {
            text[i] = buffRef.readLine();
            if (text[i].equals(DELIMITER_STRING)) {
                break;
            }
        }

        System.out.println("Wrote " + countWord(text) + " word(s).");
        System.out.println("Wrote " + countVowels(text) + " vowel(s).");
        System.out.println("Wrote " + countChar(text) + " character(s).");
    }
}
