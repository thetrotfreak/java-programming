import static java.lang.Math.abs;

import java.util.Scanner;

// 20.5.2021

public class J1 {

    public static void getSign(int number) {

        if (number > 0)
            System.out.println("It is a positive number.");
        if (number < 0)
            System.out.println("It is a negative number.");
    }

    public static void getODDorEVEN(int number) {

        if (number % 2 == 0)
            System.out.println("It is even.");
        else
            System.out.println("It is odd.");

    }

    public static void getDigitAndPlace(int number) {

        number = abs(number);
        System.out.println("It has " + Integer.toString(number).length() + " digits.");
        int place = 1;

        do {
            int digit = number % 10;
            System.out.println(digit + " in " + place + "'s position.");
            number = number / 10;
            place = place * 10;
        } while (number != 0);
    }

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Number?");
        int number = scan.nextInt();
        scan.close();

        getDigitAndPlace(number);
        getSign(number);
        getODDorEVEN(number);
    }
}