// 3.6.21

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class J9 {
    public static void main(String[] args) {

        ArrayList<Integer> ilist = new ArrayList<>();
        Scanner scan = new Scanner(System.in);

        System.out.print("Array Elements : ");
        while (scan.hasNextInt())
            ilist.add(scan.nextInt());

        scan.nextLine();
        ilist.trimToSize();
        System.out.println("Press 1 for ASCENDING\nPress -1 for DESCENDING");
        System.out.print("> ");
        int n = scan.nextInt();
        scan.close();

        System.out.println("Unsorted Array : " + ilist);
        if (n == 1) {
            Collections.sort(ilist);
            System.out.println("Sorted Array (ASC.): " + ilist);
        }
        if (n == -1) {
            Collections.sort(ilist, Collections.reverseOrder());
            System.out.println("Sorted Array (DESC.): " + ilist);
        }
    }
}