import java.util.Scanner;

public class J14 {

    public static void main(String[] args) {

        try (Scanner scan = new Scanner(System.in)) {

            System.out.print("Numerator >");
            String numerator = scan.nextLine();

            System.out.print("Denominator >");
            String denominator = scan.nextLine();

            int p = Integer.parseInt(numerator);
            int q = Integer.parseInt(denominator);

            int[] f = { p / q };
            System.out.println(f[f[0]]);

        } catch (NumberFormatException e) {
            System.out.println("INVALID ARGUMENT : EXPECTED int GOT String");
        } catch (ArithmeticException e) {
            System.out.println("DIVISION BY ZERO IS UNDEFINED");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("EXPECTED A ZERO FRACTION");
        }
    }
}
