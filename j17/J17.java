package j17;

import java.util.Scanner;
import static java.lang.System.out;

public class J17 {
    public static void main(String[] args) {

        try (Scanner scan = new Scanner(System.in)) {

            long n, d;

            out.println("A Rational?");
            n = scan.nextLong();
            d = scan.nextLong();
            Rational r1 = new Rational(n, d);

            out.println("Another Rational?");
            n = scan.nextLong();
            d = scan.nextLong();
            Rational r2 = new Rational(n, d);

            out.print("First Rational = ");
            r1.print();

            out.print("Second Rational = ");
            r2.print();

            // out.println();
            // out.print("Addition = ");
            // Rational r = r1.addRational(r2);
            // r.print();
            // out.print("Reduced Addition = ");
            // r.reduce().print();

            // out.println();
            // out.print("Subtraction = ");
            // r = r1.subtractRational(r2);
            // r.print();
            // out.print("Reduced Subtraction = ");
            // r.reduce().print();

            // out.println();
            // out.print("Multiplication = ");
            // Rational r = r1.multiplyRational(r2);
            // r.print();
            // out.print("Reduced Multiplication = ");
            // r.reduce().print();

            // out.println();
            // out.print("Division = ");
            // r = r1.divideRational(r2);
            // r.print();
            // out.print("Reduced Division = ");
            // r.reduce().print();
            out.println("Multiplication = ");
            r1.multiplyRational(r2).reduce().print();

            out.println("Division = ");
            r1.divideRational(r2).reduce().print();
        } catch (IllegalArgumentException e) {
            out.println(e);
        }
    }
}
