package j17;

import static j3.NumberUtility.gcd;
import static j3.NumberUtility.lcm;

public final class Rational {

    private final long numerator;
    private final long denominator;

    public Rational(long numerator, long denominator) throws IllegalArgumentException {

        if (denominator == 0) {
            final String message = "Argument denomintaor of type long must be non-zero";
            throw new IllegalArgumentException(message);
        }

        if (denominator < 0) {
            this.numerator = numerator * -1L;
            this.denominator = denominator * -1L;
        } else {
            this.numerator = numerator;
            this.denominator = denominator;
        }
    }

    public Rational addRational(Rational that) {

        if (this.hasEqualDenominator(that)) {
            return new Rational(this.numerator + that.numerator, this.denominator);
        }

        long newDenomintor = lcm(this.denominator, that.denominator);
        long newNumeratorThis = this.numerator;
        long newNumeratorThat = that.numerator;

        newNumeratorThis *= (newDenomintor / this.denominator);
        newNumeratorThat *= (newDenomintor / that.denominator);

        return new Rational(newNumeratorThis + newNumeratorThat, newDenomintor);
    }

    public Rational subtractRational(Rational that) {

        if (this.hasEqualDenominator(that)) {
            return new Rational((this.numerator - that.numerator), this.denominator);
        }

        long newDenomintor = lcm(this.denominator, that.denominator);
        long newNumeratorThis = this.numerator;
        long newNumeratorThat = that.numerator;

        newNumeratorThis *= (newDenomintor / this.denominator);
        newNumeratorThat *= (newDenomintor / that.denominator);

        return new Rational(newNumeratorThis - newNumeratorThat, newDenomintor);
    }

    public Rational multiplyRational(Rational that) {
        return new Rational(this.numerator * that.numerator, this.denominator * that.denominator);
    }

    public Rational divideRational(Rational that) {
        return new Rational(this.numerator * that.denominator, this.denominator * that.numerator);
    }

    public Rational reduce() {

        long newNumerator = this.numerator;
        long newDenominator = this.denominator;
        long gcd = gcd(newNumerator, newDenominator);

        // the check, gcd != 1 handles fraction having coprimes
        // no point in dividing a number by 1
        // the MOD % operator will always return 0 in such cases
        // leading to an infinite loop

        // hack maybe gcd will divide a number only once
        // while (newNumerator % gcd == 0 && newDenominator % gcd == 0 && gcd != 1) {
        // newNumerator = newNumerator / gcd;
        // newDenominator = newDenominator / gcd;
        // }
        if (gcd != 1)
            return new Rational(newNumerator / gcd, newDenominator / gcd);
        else
            return this;
    }

    private boolean hasEqualDenominator(Rational that) {
        return (this.denominator == that.denominator);
    }

    public void print() {
        System.out.println(this.numerator + "/" + this.denominator);
    }

}
