// Floyd's Trinagle
public class J31 {
    public static void main(String[] args) {
        int row = 20;
        int rowCopy = row;
        int nn = 1;
        int rightEdgeDiff = 2;

        // nn = number at nth row, nth column
        // 1
        // 2 3
        // 4 5 6
        // 7 8 9 10
        // e.g 10 = number at 4th row, 4th column
        // 1, 3, 6, 10 are numbers along the right edge

        while (row > 1) {
            nn = nn + rightEdgeDiff;
            rightEdgeDiff = rightEdgeDiff + 1;
            row--;
        }

        String fmt = "%-" + (Integer.toString(nn).length() + 1) + "d";
        int n = 1;

        System.out.println("---Floyd's Trinagle---");
        for (int i = 0; i < rowCopy; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.format(fmt, n++);
            }
            System.out.println();
        }
    }
}
