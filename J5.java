// 28.5.21
public class J5 {

    public static int gcd(int number1, int number2) {

        if (number1 == 0 || number2 == 0)
            return number2;

        if (number2 == 0)
            return number1;

        if (number1 == number2)
            return number1;

        return gcd(number2, number1 % number2);
    }

    public static void main(String[] args) {

        try {

            int number1 = Integer.parseInt(args[0]);
            int number2 = Integer.parseInt(args[1]);

            int gcd = gcd(number1, number2);
            int lcm = (number1 * number2) / gcd;

            if (gcd == 1) {
                System.out.println(number1 + " & " + number2 + " are CO-PRIME.");
                System.out.println("LCM(" + number1 + ", " + number2 + ") = " + lcm);
            } else {
                System.out.println(number1 + " & " + number2 + " are not CO-PRIME.");
                System.out.println("GCD(" + number1 + ", " + number2 + ") = " + gcd);
                System.out.println("LCM(" + number1 + ", " + number2 + ") = " + lcm);
            }

        } catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
            System.out.println("Invalid or No Inputs!");
        }
    }
}
